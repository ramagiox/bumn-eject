import React from "react";
import { StatusBar, Image, MapView, AsyncStorage, Alert, TextInput, ImageBackground } from "react-native";
import {
    Button,
    View,
    Text,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Footer,
    FooterTab,
    H1,
    Thumbnail,
    Form,
    Item,
    Label,
    Input

} from "native-base";

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';





export default class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            bumnId: this.props.navigation.state.params.bumnId,
            nik: "",
            password: ""
        }
    }


    render() {
        console.log(this.state.bumnId);
        const {goBack} = this.props.navigation;
        return (
            <Container style = {{marginTop : 25}}>

                <ImageBackground source={require('../../img/icon/regular/signin-bg.jpg')} style={{
                    flex: 1,
                    width: null,
                    height: null
                }}>
                <Header 
                        style={{ backgroundColor: "transparent",
                        shadowColor: 'transparent',
                        shadowRadius: 0,
                        borderBottomWidth: 0,
                        elevation: 0,
                        shadowOpacity: 0,
                        shadowOffset: {
                                height: 0,
                                    }
                        }}>
                    <Left>
                        <Icon name="arrow-back" onPress={() => goBack(null)} style={{color : "#fff" }} />
                    </Left>
                    <Body>
                    </Body>
                    <Right>
                    </Right>

                </Header>

                    <Content style={{ }}  >

                        <Form>
                            {/* <Text style={{ height: 450 }} /> */}
                            {/*<Image source={{ uri: "url(/my-image.png)" }}
                            style={{ height: 250, width: 250, marginLeft: "33%", flex: 1, marginTop: 75, marginBottom: 75 }} />*/}

                            <Label style={{ marginTop: "15%", textAlign: 'center', fontSize: responsiveFontSize(4), color: "white" }}>Selamat Datang!</Label>
                            <Label style={{ marginTop: "10%", textAlign: 'left', fontSize: responsiveFontSize(2), color: "white", marginLeft: "10%", marginRight: "10%" }}>Silahkan masuk dengan menginputkan NIK dan password Anda</Label>


                            <Item style={{ backgroundColor: 'rgba(255, 255, 255, 0.2)', paddingLeft: "3%", marginRight: "10%", marginLeft: "10%", marginTop: "15%", height: responsiveHeight(7), borderRadius: 5 }}>
                                <Image style={{ paddingRight: "5%" }} source={require('../../img/asset/ic-idcard.png')}  />
                                <Input onChangeText={this.handleNik} style={{ color: "white",marginLeft:"5%" }} placeholderTextColor="white" placeholder="NIK" />
                            </Item>

                            <Item style={{ backgroundColor: 'rgba(255, 255, 255, 0.2)', paddingLeft: "3%", marginRight: "10%", marginLeft: "10%", marginTop: "3%", height: responsiveHeight(7), borderRadius: 5 }}>
                                 <Image style={{ paddingRight: "5%" }} source={require('../../img/asset/ic-password.png')}  />
                                <Input onChangeText={this.handlePassword} secureTextEntry={true} style={{ color: "white",marginLeft:"6%" }} placeholderTextColor="white" placeholder="Password" />
                            </Item>

                            <Button bordered light style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: "10%", marginRight: "30%", marginLeft: "30%", width: responsiveWidth(40) }}
                                onPress={this.login} >
                                <Text style={{}}>Masuk</Text>
                            </Button>
                        
                        </Form>
                    </Content>
                </ImageBackground>
            </Container>
        );
    }

    handleNik = (text) => {
        this.setState({ nik: text })
    }
    handlePassword = (text) => {
        this.setState({ password: text })
    }

    login = () => {
        if (this.state.nik == null || this.state.nik == "" || this.state.password == null || this.state.password == "") {
            Alert.alert(
                'Pesan',
                'NIK / Password tidak boleh kosong',
                [
                    //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                    //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    { text: 'OK', style: 'cancel' },
                ],
                { cancelable: false }
            )
        } else {

            return fetch("http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/login?officer=true", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
                },
                body: JSON.stringify({
                    nik: this.state.nik,
                    password: this.state.password,
                    bumnId : this.state.bumnId
                })
            })
                .then(response => response.json())
                .then((data) => {
                    console.log(data.data.userId);
                    AsyncStorage.setItem("token", data.data.token);
                    AsyncStorage.setItem("bumnId",this.state.bumnId);
                    AsyncStorage.setItem("userid", data.data.userId);
                    AsyncStorage.getItem("token", (error, result) => {
                        if (result) {
                            console.log("token : " + result)

                        }
                        if (result == null||result=="") {

                            Alert.alert(
                                'Pesan',
                                'NIK / Password tidak terdaftar',
                                [
                                    //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                                    //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                    { text: 'OK', style: 'cancel' },
                                ],
                                { cancelable: false }
                            )
                        } else {
                                    this.props.navigation.navigate("LoginProfil");
                         
                        }
                    })



                })
                .catch(error => {
                     Alert.alert(
                                'Pesan',
                                'NIK / Password tidak terdaftar',
                                [
                                    //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                                    //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                    { text: 'OK', style: 'cancel' },
                                ],
                                { cancelable: false }
                            )
                });

        }
    }
}



