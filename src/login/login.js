import React from "react";
import { StatusBar, Image, MapView, AsyncStorage, Alert, TextInput, ImageBackground } from "react-native";
import {
    Button,
	View,
    Text,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Footer,
    FooterTab,
    H1,
    Thumbnail,
    Form,
    Item,
    Label,
    Input

} from "native-base";

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';





export default class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            username: "",
            password: ""
        }
    }


    render() {
        return (
            <Container>
			
				 <ImageBackground  source={require('../../img/icon/regular/signin-bg.png')} style={{flex: 1,
width: null,
height: null}}>
                    
					
                <Content padder style={{ }}  >
				

					
                    <Form>
                        {/* <Text style={{ height: 450 }} /> */}
                        {/*<Image source={{ uri: "url(/my-image.png)" }}
                            style={{ height: 250, width: 250, marginLeft: "33%", flex: 1, marginTop: 75, marginBottom: 75 }} />*/}

                        <Label style={{ marginTop: "40%", textAlign: 'center', fontSize: responsiveFontSize(4), color: "white" }}>Selamat Datang!</Label>

                        <Item style={{ backgroundColor: 'rgba(255, 255, 255, 0.2)', paddingLeft: "3%", marginRight: "10%", marginLeft: "10%", marginTop: "15%", height: responsiveHeight(7), borderRadius: 5 }}>
                            <Icon style={{ paddingRight: "5%" }} active name='home' />
                            <Input style={{ color: "white" }} placeholderTextColor="white" placeholder="NIK" />
                        </Item>

                        <Item style={{ backgroundColor: 'rgba(255, 255, 255, 0.2)', paddingLeft: "3%", marginRight: "10%", marginLeft: "10%", marginTop: "3%", height: responsiveHeight(7), borderRadius: 5 }}>
                            <Icon style={{ paddingRight: "5%" }} active name='home' />
                            <Input secureTextEntry={true} style={{ color: "white" }} placeholderTextColor="white" placeholder="Password" />
                        </Item>

                        <Label style={{ marginTop: "1%", textAlign: 'right', fontSize: responsiveFontSize(2), color: "white", marginRight: "10%", fontStyle: "italic" }} >Lupa Password</Label>

                        <Button bordered light style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: "10%",marginRight:"30%",marginLeft:"30%",width:responsiveWidth(40)}}
                         onPress={() => this.props.navigation.navigate("LoginProfil")} >
                            <Text style={{}}>Masuk</Text>
                        </Button>
                      
                        <Label style={{ marginTop:"40%", textAlign: 'center', fontSize: responsiveFontSize(2), color: "white", fontWeight: "bold" }} >Tidak bisa masuk? Hubungi kami</Label>

                        {/*<Item rounded floatingLabel style={{ backgroundColor: "white", width : '50%', marginLeft:"25%", paddingLeft: 25}}>
                            <Label style={{ paddingLeft: 25 }}>Username</Label>
                            <Input onChangeText={this.handleUsername}/>
                        </Item>
                        <Item rounded floatingLabel style={{ backgroundColor: "white", width : '50%', marginLeft:"25%", paddingLeft: 25 }}>
                            <Label style={{ paddingLeft: 25 }}>Password</Label>
                            <Input secureTextEntry={true} onChangeText={this.handlePassword} />
                        </Item>
                        <Button onPress={this.login}
                            full rounded light style={{ backgroundColor: "white", width : '50%', marginLeft:"25%", marginTop: 50, height: 50 }}>
                            <Text >Login</Text>
                        </Button>
                        <Button full transparent style={{ width : '50%', marginLeft:"25%", marginTop: 50, height: 50 }}
                            onPress={() => this.props.navigation.navigate("Register")}>
                            <Text >Not a member? Sign up now.</Text>
                        </Button>*/}
                    </Form>
                </Content> 
				</ImageBackground>
            </Container>
        );
    }

    handleUsername = (text) => {
        this.setState({ username: text })
    }
    handlePassword = (text) => {
        this.setState({ password: text })
    }

    login = () => {
        return fetch("https://penyewaanbatch124.herokuapp.com/api/login/authenticate", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                UserNamePenyewa: this.state.username,
                PasswordPenyewa: this.state.password
            })
        })
            .then(response => response.json())
            .then((data) => {
                console.log(data);
                AsyncStorage.setItem("username", this.state.username);
                AsyncStorage.setItem("token", data)
                AsyncStorage.getItem("token", (error, result) => {
                    if (result) {
                        console.log("token : " + result)
                    }
                    console.log("token2 : " + result);
                    if (result == null) {

                        Alert.alert(
                            'Pesan',
                            'wrong username/password',
                            [
                                //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                                //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                { text: 'OK', style: 'cancel' },
                            ],
                            { cancelable: false }
                        )
                    } else {
                        Alert.alert(
                            'Pesan',
                            'Login Berhasil',
                            [
                                //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                                //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                {
                                    text: 'OK', onPress: () => {
                                        this.props.navigation.navigate("Home");
                                        this.setState(this.state);
                                    }
                                },
                            ],
                            { cancelable: false }
                        )
                    }
                })



            })

    }

}



