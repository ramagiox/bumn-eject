import React from "react";
import { StatusBar, Image, MapView, AsyncStorage, Alert, TouchableOpacity } from "react-native";
import {
	Button,
	Text,
	Container,
	Card,
	CardItem,
	Body,
	Content,
	Header,
	Title,
	Left,
	Icon,
	Right,
	Footer,
	FooterTab,
	H1, H2,
	Thumbnail,
	Form,
	Item,
	Label,
	Input,
	List,
	ListItem,
	View

} from "native-base";

import Modal from 'react-native-modal';



export default class Booking extends React.Component {

	constructor() {
		super()
		this.state = {
			dataBooking: [],
			dataBarang: [],
			dataSewaBooking:[],
			username: "",
			dataBarangDetail: [],
			dataSewaDetail: [],
			dataBarang_id: "",
			KdBarang: "",
			NamaBarang: "",
			KdKategori: "",
			HargaSewa: Number,
			HargaDenda: Number,
			StatusBarang: "",
			Foto: "",
			JumlahBarang: Number

		}
	}




	_hideModal = () => this.setState({ isModalVisible: false })


	componentDidMount() {
		AsyncStorage.getItem("username", (error, result) => {
			if (result) {
				console.log("username : " + result)
			}
			this.state.username = result;

			AsyncStorage.getItem("token", (error, result2) => {
				fetch("https://penyewaanbatch124.herokuapp.com/api/datasewa/search2/" + result + "?token=" + result2, {
					method: "GET"
				})
					.then((response) => response.json())
					.then((data) => {

						this.setState({
							dataBooking: data
						});
						console.log(dataBooking)
						
					})
					.catch((error) => {
						console.log(error);
					})

			})
		})

	}




	render() {
		return (
			<Container>

			</Container>
		);
	}



	_showModal(id,KdBarang) {
		AsyncStorage.getItem("token", (error, result) => {
			if (result) {

				fetch("https://penyewaanbatch124.herokuapp.com/api/datasewa/" + id + "?token=" + result, {
					method: "GET"
				})
					.then((response) => response.json())
					.then((data) => {

						this.setState({
							dataSewaBooking: data

						});
						console.log(this.state.dataSewaBooking)
						this.state.dataSewaBooking.TglMulai=this.state.dataSewaBooking.TglMulai.toString().slice(0,10)
						this.state.dataSewaBooking.TglSelesai=this.state.dataSewaBooking.TglSelesai.toString().slice(0,10)

										fetch("https://penyewaanbatch124.herokuapp.com/api/kdbarang/" + KdBarang, {
											method: "GET"
										})
											.then((response) => response.json())
											.then((data) => {
						
												this.setState({
													dataBarang: data
						
												});
						
												console.log(data);
											})
											.catch((error) => {
												console.log(error);
											})
				
					})
					.catch((error) => {
						console.log(error);
					})
				


			}
		})
		this.setState({ isModalVisible: true })

	}


	dataSewaDelete(id, KdBarang) {
		AsyncStorage.getItem("token", (error, result) => {
			console.log(id+"-"+KdBarang);
			fetch("https://penyewaanbatch124.herokuapp.com/api/kdbarang/" + KdBarang, {
				method: "GET"
			})
				.then((response) => response.json())
				.then((data) => {

					this.setState({
						dataBarangDetail: data[0],
						dataBarang_id: data[0]._id,
						KdBarang: data[0].KdBarang,
						NamaBarang: data[0].NamaBarang,
						KdKategori: data[0].KdKategori,
						HargaSewa: data[0].HargaSewa,
						StatusBarang: data[0].StatusBarang,
						JumlahBarang: data[0].JumlahBarang,
						HargaDenda: data[0].HargaDenda,
						Foto: data[0].Foto



					});

					console.log(this.state.dataBarangDetail);
					console.log("pert : " + this.state.JumlahBarang)
					console.log("id : " + this.state.dataBarang_id);
					

					fetch("https://penyewaanbatch124.herokuapp.com/api/datasewa/" + id+"?token="+result, {
						method: "GET"
					})
						.then((response) => response.json())
						.then((data) => {

							this.setState({
								dataSewaDetail: data,
								JumlahBarangDetail: data.JumlahBarang

							});
							console.log(this.state.dataSewaDetail)
							console.log("kedua : " + this.state.JumlahBarangDetail)
							this.state.JumlahBarang = this.state.JumlahBarang + this.state.JumlahBarangDetail;
							console.log("total : " + this.state.JumlahBarang)

							fetch("https://penyewaanbatch124.herokuapp.com/api/barang/" + this.state.dataBarang_id, {
								method: "PUT",
								headers: {
									'Content-Type': 'application/json',
									'Accept': 'application/json'
								},
								body: JSON.stringify({
									KdBarang: this.state.KdBarang,
									NamaBarang: this.state.NamaBarang,
									KdKategori: this.state.KdKategori,
									HargaSewa: this.state.HargaSewa,
									HargaDenda: this.state.HargaDenda,
									StatusBarang: this.state.StatusBarang,
									JumlahBarang: this.state.JumlahBarang,
									Foto: this.state.Foto
								})
							})

								.then(response => response.json())


								.then(
								fetch("https://penyewaanbatch124.herokuapp.com/api/datasewa/" + id+"?token="+result, {
									method: "DELETE"
								})
									.then(
									Alert.alert(
										'Cancel Booking',
										'Anda Telah membatalkan Peminjaman',
										[
											//{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
											//{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
											{
												text: 'OK', onPress: () => {

													this.setState({ isModalVisible: false });

													

												}
											},
										],
										{ cancelable: false }
									)
									)
								)



						})
						.catch((error) => {
							console.log(error);
						})

				})
				.catch((error) => {
					console.log(error);
				})






		})
	}




}





