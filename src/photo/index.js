import React, { Component } from "react";
import {
  ActivityIndicator,
  Clipboard,
  Image,
  Share,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import {
  Tab,
  Button,
  Tabs,
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Title,
  Left,
  Icon,
  Right,
  Footer,
  FooterTab,
  H1,
  Thumbnail,
  Form,
  Item,
  Label,
  Input,
  AsyncStorage
} from 'native-base';
import Exponent, { Constants, ImagePicker, registerRootComponent } from 'expo';



export default class Index1 extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      nomor_laporan: this.props.navigation.state.params.nomor_laporan,
      status1: true,
      status2: true,
      uploading: false
    }
  }


  render() {
    let { image } = this.state;
    let { galeri } = this.state;
    return (
      <Container>
        <Header style={{ marginTop: 25, alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff' }}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("BerandaDetail", { nomor_laporan: this.state.nomor_laporan })}
            >
              <Icon style={{ color: "#000" }} name="arrow-back" />
            </Button>
          </Left>
            <Title style={{ color: "#000", alignItems: "center" }}>Tambah Eviden</Title>
          <Right />
        </Header>
        <Tabs initialPage={0} tabBarUnderlineStyle={{ borderBottomWidth: 2 }}>
          <Tab heading="Kamera" tabStyle={{ backgroundColor: 'white' }} textStyle={{ color: '#012' }} activeTabStyle={{ backgroundColor: '#fff' }} activeTextStyle={{ color: '#00f', fontWeight: 'normal' }}>

            <Container>
              <View style={{ width: '100%', height: '50%' }}>
                {image &&
                  <Image style={{ width: '100%', height: '95%' }} source={{ uri: image }} />}
              </View>
              <View style={{ flex: 1, alignItems: 'center' }}>
                {
                  this.state.status1 ?
                    <View >
                      <TouchableOpacity onPress={this._takePhoto}>
                        <Image
                          source={require("../../img/asset/take.png")} />
                      </TouchableOpacity>
                    </View> : null
                }
              </View>
              {
                this.state.image ?
                  <View style={{ alignItems: 'center', bottom: '20%' }}>
                    <Text style={{ fontSize: 17 }}> Gunakan foto ini? </Text>
                  </View> : null
              }
              {
                this.state.image ?
                  <View style={{ flexDirection: "row", flex: 1, bottom: '25%', left: 0, right: 0, justifyContent: 'space-between', padding: 15 }}>
                    <Button
                      style={{ width: '45%', justifyContent: 'center', border: '5%' }}
                      onPress={() => this.props.navigation.navigate("Detail", { imagesdata: this.state.image, nomor_laporan: this.state.nomor_laporan })}>
                      <Text style={{ fontSize: 17, color: '#fff' }}> Gunakan</Text>
                    </Button>
                    <Button bordered primary
                      style={{ width: '45%', justifyContent: 'center', borderWidth: 1, borderColor: 'blue' }}
                      light onPress={this._takePhoto}>
                      <Text style={{ fontSize: 17, color: '#00f' }}> Ambil Lagi</Text>
                    </Button>
                  </View> : null
              }
            </Container>
          </Tab>
          <Tab heading="Galeri" tabStyle={{ backgroundColor: 'white' }} textStyle={{ color: '#012' }} activeTabStyle={{ backgroundColor: '#fff' }} activeTextStyle={{ color: '#00f', fontWeight: 'normal' }}>
            <View style={{ width: '100%', height: '50%' }}>
              <Image style={{ width: '100%', height: '95%' }} source={{ uri: this.state.galeri }} />
            </View>
            {
              this.state.status2 ?
                <Button
                  style={{ backgroundColor: "#004ecc", marginLeft: '10%', bottom: 0, width: "80%", justifyContent: 'center', alignItems: 'center' }}
                  transparent onPress={this._pickImage}>
                  <Text style={{ color: '#fff' }}>Pick an image from Galery</Text>
                </Button> : null
            }

            {
              this.state.galeri ?
                <View style={{ alignItems: 'center', bottom: 0 }}>
                  <Text style={{ fontSize: 20 }}> Gunakan foto ini? </Text>
                </View> : null
            }
            {
              this.state.galeri ?
                <View style={{ flexDirection: "row", flex: 1, bottom: 0, left: 0, right: 0, justifyContent: 'space-between', padding: 15 }}>
                  <Button
                    style={{ width: '45%', justifyContent: 'center', border: '5%' }}
                    onPress={() => this.props.navigation.navigate("Detail", { imagesdata: this.state.galeri, nomor_laporan: this.state.nomor_laporan })}>
                    <Text style={{ fontSize: 17, color: '#fff' }}> Gunakan</Text>
                  </Button>
                  <Button bordered primary
                    style={{ width: '45%', justifyContent: 'center', borderWidth: 1, borderColor: 'blue' }}
                    light onPress={this._pickImage}>
                    <Text style={{ fontSize: 17, color: '#00f' }}> Ambil Lagi</Text>
                  </Button>
                </View> : null
            }
          </Tab>
        </Tabs>
      </Container>
    );
  }

  _takePhoto = async () => {
    console.log('nolaporan' + this.state.nomor_laporan);
    let pickerResult = await ImagePicker.launchCameraAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });
    if (pickerResult != undefined) {
      this.setState({ status: true })
    }
    else {
      this.setState({ status: false })
    }
    if (!pickerResult.cancelled) {
      this.setState({ image: pickerResult.uri });
      console.log(this.state.image);
      this.setState({ status1: false })
    }
  };

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });

    if (result != undefined) {
      this.setState({ status3: true })
    }
    else {
      this.setState({ status3: false })
    }
    if (!result.cancelled) {
      this.setState({ galeri: result.uri });
      console.log(this.state.galeri);
      this.setState({ status2: false })

    }
  };
}