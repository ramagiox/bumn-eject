import React from "react";
import { StatusBar, Image, MapView, AsyncStorage, Alert, TouchableOpacity } from "react-native";
import {
	Button,
	Text,
	Container,
	Card,
	CardItem,
	Body,
	Content,
	Header,
	Title,
	Left,
	Icon,
	Right,
	Footer,
	FooterTab,
	H1, H2,
	Thumbnail,
	Form,
	Item,
	Label,
	Input,
	List,
	ListItem,
	View

} from "native-base";

import Modal from 'react-native-modal';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';



export default class Selesai extends React.Component {

	constructor() {
		super()
		this.state = {
			dataSelesai: [],
			dataBarang: [],
			dataSewaSelesai:[],
			username: "",
			dataBarangDetail: [],
			dataSewaDetail: [],
			dataBarang_id: "",
			KdBarang: "",
			NamaBarang: "",
			KdKategori: "",
			HargaSewa: Number,
			HargaDenda: Number,
			StatusBarang: "",
			Foto: "",
			JumlahBarang: Number,

		}
	}




	_hideModal = () => this.setState({ isModalVisible: false })


	componentDidMount() {
		 AsyncStorage.getItem("bumnId", (error, result) => {
		 fetch("http://ec2-13-250-62-76.ap-southeast-1.compute.amazonaws.com:7000/api/v1/laporan/byStatusSolved", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                instansi : result
            })
        })
            .then(response => response.json())
            .then((data) => {
                console.log(data.response);
                this.setState({
							dataSelesai: data.response

						});
						console.log(this.state.dataSelesai)

            })
		 })

	}




	render() {
	//	const { navigate } = this.props.navigation;
		return (
				<Container>

				<Content>
					<Card style={{ backgroundColor: 'powderblue',paddingTop:"2%", marginRight: "5%", marginTop: "5%", marginLeft: "5%",height:responsiveWidth(20) }}>
						<CardItem style={{ backgroundColor: 'powderblue' }}>
							<Left>
								<Thumbnail square small
									source={require("../../img/asset/ic-party.png")}
								/>
								<Body>
									<Text style={{ fontSize: responsiveFontSize(1.5),  marginLeft: "10%" }}>Selamat! berikut tiket pelaporan Anda yang berstatus <Text style={{fontWeight:"bold"}}>Solved.</Text> </Text>
								

								</Body>
							</Left>
						</CardItem>
					</Card>

{
							this.state.dataSelesai.map((item, index) => (
					<List >
						
						<ListItem style={{borderBottomWidth: 0}}>
			
							<Left>
								<Thumbnail small
									source={require("../../img/asset/Profil.png")}
								/>
								<Body>
									<Text style={{ fontSize: responsiveFontSize(2) }}>{item.pelapor}</Text>
									<Text style={{ fontSize: responsiveFontSize(1.5) }}>No. Tiket : {item.nomor_laporan}</Text>
								</Body>
							</Left>
							<Right>
								<Text style={{ fontSize: responsiveFontSize(1.5),fontStyle:"italic" }}>{item.tanggal_laporan}</Text>
							</Right>
				

						</ListItem>
						
						
						<ListItem style={{marginTop:"-5%",marginRight:"5%"}} >
							<Text style={{fontSize: responsiveFontSize(2)}}>{item.description}</Text>
						</ListItem>
						
					</List>
					))
						} 

					
				</Content>
			</Container>
		);
	}



	_showModal(id,KdBarang) {
		AsyncStorage.getItem("token", (error, result) => {
			if (result) {

				fetch("https://penyewaanbatch124.herokuapp.com/api/datasewa/" + id + "?token=" + result, {
					method: "GET"
				})
					.then((response) => response.json())
					.then((data) => {

						this.setState({
							dataSewaSelesai: data

						});
						console.log(this.state.dataSewaSelesai)
						this.state.dataSewaSelesai.TglMulai=this.state.dataSewaSelesai.TglMulai.toString().slice(0,10)
						this.state.dataSewaSelesai.TglSelesai=this.state.dataSewaSelesai.TglSelesai.toString().slice(0,10)

										fetch("https://penyewaanbatch124.herokuapp.com/api/kdbarang/" + KdBarang, {
											method: "GET"
										})
											.then((response) => response.json())
											.then((data) => {
						
												this.setState({
													dataBarang: data
						
												});
						
												console.log(data);
											})
											.catch((error) => {
												console.log(error);
											})
				
					})
					.catch((error) => {
						console.log(error);
					})
				


			}
		})
		this.setState({ isModalVisible: true })

	}


	dataSewaDelete(id, KdBarang) {
		AsyncStorage.getItem("token", (error, result) => {
			console.log(id+"-"+KdBarang);
			fetch("https://penyewaanbatch124.herokuapp.com/api/kdbarang/" + KdBarang, {
				method: "GET"
			})
				.then((response) => response.json())
				.then((data) => {

					this.setState({
						dataBarangDetail: data[0],
						dataBarang_id: data[0]._id,
						KdBarang: data[0].KdBarang,
						NamaBarang: data[0].NamaBarang,
						KdKategori: data[0].KdKategori,
						HargaSewa: data[0].HargaSewa,
						StatusBarang: data[0].StatusBarang,
						JumlahBarang: data[0].JumlahBarang,
						HargaDenda: data[0].HargaDenda,
						Foto: data[0].Foto



					});

					console.log(this.state.dataBarangDetail);
					console.log("pert : " + this.state.JumlahBarang)
					console.log("id : " + this.state.dataBarang_id);
					

					fetch("https://penyewaanbatch124.herokuapp.com/api/datasewa/" + id+"?token="+result, {
						method: "GET"
					})
						.then((response) => response.json())
						.then((data) => {

							this.setState({
								dataSewaDetail: data,
								JumlahBarangDetail: data.JumlahBarang

							});
							console.log(this.state.dataSewaDetail)
							console.log("kedua : " + this.state.JumlahBarangDetail)
							this.state.JumlahBarang = this.state.JumlahBarang + this.state.JumlahBarangDetail;
							console.log("total : " + this.state.JumlahBarang)

							fetch("https://penyewaanbatch124.herokuapp.com/api/barang/" + this.state.dataBarang_id, {
								method: "PUT",
								headers: {
									'Content-Type': 'application/json',
									'Accept': 'application/json'
								},
								body: JSON.stringify({
									KdBarang: this.state.KdBarang,
									NamaBarang: this.state.NamaBarang,
									KdKategori: this.state.KdKategori,
									HargaSewa: this.state.HargaSewa,
									HargaDenda: this.state.HargaDenda,
									StatusBarang: this.state.StatusBarang,
									JumlahBarang: this.state.JumlahBarang,
									Foto: this.state.Foto
								})
							})

								.then(response => response.json())


								.then(
								fetch("https://penyewaanbatch124.herokuapp.com/api/datasewa/" + id+"?token="+result, {
									method: "DELETE"
								})
									.then(
									Alert.alert(
										'Cancel Selesai',
										'Anda Telah membatalkan Peminjaman',
										[
											//{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
											//{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
											{
												text: 'OK', onPress: () => {

													this.setState({ isModalVisible: false });

													

												}
											},
										],
										{ cancelable: false }
									)
									)
								)



						})
						.catch((error) => {
							console.log(error);
						})

				})
				.catch((error) => {
					console.log(error);
				})
		})
	}
}





