import React from "react";
import { StatusBar, Image, MapView, AsyncStorage, Alert, TouchableOpacity} from "react-native";
import {
    Button,
    Text,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Footer,
    FooterTab,
    H1,H2,
    Thumbnail,
    Form,
    Item,
    Label,
    Input,
	List,
	ListItem,
	View

} from "native-base";

import Modal from 'react-native-modal';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';


export default class Closed extends React.Component {

    constructor(){
        super()
        this.state = {     
            dataClosed : [],
			dataBarang : [],
			dataSewaClosed : [],
			today : new Date(),
			denda : Number,
			dendast : "",
			username : ""
        }
    }
	
	
	

    _hideModal = () => this.setState({ isModalVisible: false })
	
	
	componentDidMount(){
		AsyncStorage.getItem("username", (error, result) => {
            if (result) {
                console.log("username : " + result)
            }
			this.state.username = result;
			
			AsyncStorage.getItem("token", (error, result2) => {
			fetch("https://penyewaanbatch124.herokuapp.com/api/datasewa/search3/"+result+"?token="+result2,{
            method:"GET"
			})
			.then((response) => response.json())
			.then((data) => {
            
            this.setState({
                dataClosed : data
            });
			})
			.catch((error)=>{
				console.log(error);
			})
	
			})
        })
        
    }

    render() {
        return (
      <Container>

				<Content>
					<Card style={{ backgroundColor: 'powderblue',paddingTop:"2%", marginRight: "5%", marginTop: "5%", marginLeft: "5%",height:responsiveWidth(20) }}>
						<CardItem style={{ backgroundColor: 'powderblue' }}>
							<Left>
								<Thumbnail square small
									source={require("../../img/asset/ic-party.png")}
								/>
								<Body>
									<Text style={{ fontSize: responsiveFontSize(1.5),  marginLeft: "10%" }}>Lihat apa kata mereka tentang pekerjaan Anda!</Text>
								

								</Body>
							</Left>
						</CardItem>
					</Card>

					<List >
						<ListItem style={{borderBottomWidth: 0}}>
							<Left>
								<Thumbnail small
									source={require("../../img/asset/Profil.png")}
								/>
								<Body>
									<Text style={{ fontSize: responsiveFontSize(2) }}>Fatia Aziza</Text>
									<Text style={{ fontSize: responsiveFontSize(1.5) }}>No. Tiket : 1022</Text>
								</Body>
							</Left>
							<Right>
								<Text style={{ fontSize: responsiveFontSize(1.5),fontStyle:"italic" }}>10 Desember</Text>
							</Right>
						</ListItem>
						<ListItem style={{marginTop:"-5%",marginRight:"5%",borderBottomWidth: 0}} >
							<Image source={require("../../img/asset/Star-kuning.png")}/>
							<Image source={require("../../img/asset/Star-kuning.png")}/>
							<Image source={require("../../img/asset/Star-kuning.png")}/>
							<Image source={require("../../img/asset/Star-kuning.png")}/>
							<Image source={require("../../img/asset/Star-kuning.png")}/>
							<Text style={{fontSize: responsiveFontSize(2),marginLeft:"5%",fontWeight:"bold"}}>5/5</Text>
							
						</ListItem>
						<ListItem style={{marginTop:"-5%",marginRight:"5%"}} >
							<Text style={{fontSize: responsiveFontSize(2)}}>Keren nih telkom, langsung gerak benerin laporan saya</Text>
						</ListItem>
					</List>

					<List >
						<ListItem style={{borderBottomWidth: 0}}>
							<Left>
								<Thumbnail small
									source={require("../../img/asset/Profil.png")}
								/>
								<Body>
									<Text style={{ fontSize: responsiveFontSize(2) }}>Fatia Aziza</Text>
									<Text style={{ fontSize: responsiveFontSize(1.5) }}>No. Tiket : 1022</Text>
								</Body>
							</Left>
							<Right>
								<Text style={{ fontSize: responsiveFontSize(1.5),fontStyle:"italic" }}>10 Desember</Text>
							</Right>
						</ListItem>
						<ListItem style={{marginTop:"-5%",marginRight:"5%",borderBottomWidth: 0}} >
							<Image source={require("../../img/asset/Star-kuning.png")}/>
							<Image source={require("../../img/asset/Star-kuning.png")}/>
							<Image source={require("../../img/asset/Star-kuning.png")}/>
							<Image source={require("../../img/asset/Star-kuning.png")}/>
							<Image source={require("../../img/asset/Star-kuning.png")}/>
							<Text style={{fontSize: responsiveFontSize(2),marginLeft:"5%",fontWeight:"bold"}}>5/5</Text>
							
						</ListItem>
						<ListItem style={{marginTop:"-5%",marginRight:"5%"}} >
							<Text style={{fontSize: responsiveFontSize(2)}}>Keren nih telkom, langsung gerak benerin laporan saya</Text>
						</ListItem>
					</List>
					<List >
						<ListItem style={{borderBottomWidth: 0}}>
							<Left>
								<Thumbnail small
									source={require("../../img/asset/Profil.png")}
								/>
								<Body>
									<Text style={{ fontSize: responsiveFontSize(2) }}>Fatia Aziza</Text>
									<Text style={{ fontSize: responsiveFontSize(1.5) }}>No. Tiket : 1022</Text>
								</Body>
							</Left>
							<Right>
								<Text style={{ fontSize: responsiveFontSize(1.5),fontStyle:"italic" }}>10 Desember</Text>
							</Right>
						</ListItem>
						<ListItem style={{marginTop:"-5%",marginRight:"5%",borderBottomWidth: 0}} >
							<Image source={require("../../img/asset/Star-kuning.png")}/>
							<Image source={require("../../img/asset/Star-kuning.png")}/>
							<Image source={require("../../img/asset/Star-kuning.png")}/>
							<Image source={require("../../img/asset/Star-kuning.png")}/>
							<Image source={require("../../img/asset/Star-kuning.png")}/>
							<Text style={{fontSize: responsiveFontSize(2),marginLeft:"5%",fontWeight:"bold"}}>5/5</Text>
							
						</ListItem>
						<ListItem style={{marginTop:"-5%",marginRight:"5%"}} >
							<Text style={{fontSize: responsiveFontSize(2)}}>Keren nih telkom, langsung gerak benerin laporan saya</Text>
						</ListItem>
					</List>
				</Content>
			</Container>
        );
    }
	


	_showModal(id,KdBarang) {
        AsyncStorage.getItem("token", (error, result) => {
            if (result) {
                fetch("https://penyewaanbatch124.herokuapp.com/api/datasewa/"+id+"?token="+result, {
                    method: "GET"
                })
                    .then((response) => response.json())
                    .then((data) => {
        
                        this.setState({
                            dataSewaClosed: data
                            
                        });
									fetch("https://penyewaanbatch124.herokuapp.com/api/kdbarang/"+KdBarang, {
								method: "GET"
								})
								.then((response) => response.json())
								.then((data) => {
					
									this.setState({
										dataBarang: data
										
									});
									
									this.state.denda=(this.state.dataBarang[0].HargaDenda * Math.floor((Date.parse(this.state.today.toString()) - Date.parse(this.state.dataSewaClosed.TglSelesai.toString())) / 86400000))
									this.state.dendast = this.state.denda.toString();
									this.state.dataSewaClosed.TglMulai=this.state.dataSewaClosed.TglMulai.toString().slice(0,10)
									this.state.dataSewaClosed.TglSelesai=this.state.dataSewaClosed.TglSelesai.toString().slice(0,10)
									console.log(this.state.dataSewaClosed.JumlahBarang);
									console.log(this.state.dendast);
									
									console.log(this.state.dataBarang[0].HargaDenda);
									console.log(Date.parse(this.state.today.toString()));
									console.log(Date.parse(this.state.dataSewaClosed.TglSelesai.toString()));
									
									console.log(data);
									this.setState({ isModalVisible: true })
								})
								.catch((error) => {
									console.log(error);
								})
        
                        console.log("data 2 : "+this.state.dataSewaAktif);
                    })
                    .catch((error) => {
                        console.log(error);
                    })
            
                
            }
        })
        
    
    }  
    

}





