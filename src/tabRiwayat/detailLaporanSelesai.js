import React from "react";
import { AppRegistry, View, StyleSheet, StatusBar, Image, Linking } from "react-native";
import {
    Button,
    Text,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Left,
    Badge,
    Right,
    Icon,
    Title,
    Input,
    InputGroup,
    Item,
    Tab,
    Tabs,
    Footer,
    FooterTab,
    Label,
    Thumbnail,
    ListItem,
    CheckBox,
    Picker,
    TouchableOpacity
} from "native-base";


import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';


export default class DetailLaporanSelesai extends React.Component {



    constructor(props) {
        super(props);
        this.state = {
            nomor_laporan: this.props.navigation.state.params.nomor_laporan,
            dataLaporanSelesai: [],
            selected: "key0",
            status: "",
            bintang1 : false,
            bintang2 : false,
            bintang3 : false,
            bintang4 : false,
            bintang5 : false,
            bintang0 : false,
            rating : 0
        };
    }

    componentWillMount() {

        fetch("http://ec2-13-250-62-76.ap-southeast-1.compute.amazonaws.com:7000/api/v1/laporan/findById/" + this.state.nomor_laporan, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then((data) => {
                
                this.setState({
                    dataLaporanSelesai: data.response[0],
                    rating : 1 
                });
                if (this.state.dataLaporanSelesai.rating == null) {
                    this.state.dataLaporanSelesai.rating = 0;
                }
                
                
        //         if (this.state.dataLaporanSelesai.rating==5) {
        //         this.state.bintang5 = true
        //   //      this.state.dataLaporanSelesai.rating = 0;
                
        //     }else if (this.state.dataLaporanSelesai.rating == 1) {
        //         this.state.bintang1 = true
        //     }else if (this.state.dataLaporanSelesai.rating == 2) {
        //        this.state.bintang2 = true
        //     }else if (this.state.dataLaporanSelesai.rating == 3) {
        //         this.state.bintang3 = true
        //     }else if (this.state.dataLaporanSelesai.rating == 4) {
        //         this.state.bintang4 = true
        //     }else if (this.state.dataLaporanSelesai.rating == 0 || this.state.dataLaporanSelesai.rating == null) {
        //         this.state.bintang0 = true
        //     }
        //     this.setState({ refreshing: true });

        //     console.log(this.state.dataLaporanSelesai.rating);
        //     console.log(this.state.bintang0)

        })
        //  if (this.state.dataLaporanSelesai.rating==5) {
        //         this.state.bintang5 = true
        //       //  this.state.dataLaporanSelesai.rating = 0;
                
        //     }else if (this.state.dataLaporanSelesai.rating == 1) {
        //         this.state.bintang1 = true
        //     }else if (this.state.dataLaporanSelesai.rating == 2) {
        //        this.state.bintang2 = true
        //     }else if (this.state.dataLaporanSelesai.rating == 3) {
        //         this.state.bintang3 = true
        //     }else if (this.state.dataLaporanSelesai.rating == 4) {
        //         this.state.bintang4 = true
        //     }else if (this.state.dataLaporanSelesai.rating == 0) {
        //         this.state.bintang0 = true
        //     }
        //     this.setState({ refreshing: true });

            
    }

      _handleOpenWithLinking = () => {
        Linking.openURL('sms:/open?addresses=&body=\Hello, Please follow the following url: \http://google.com');
    }

    ShowHideTextComponentView = () => {

        if (this.state.status == "key1") {
            this.setState({ status: false })
        }
        else {
            this.setState({ status: true })
        }
    }

    onValueChange3(value) {
        this.setState({
            selected: value
        });
        if (this.state.selected == "key1") {
            this.setState({ status: false })
        }
        else {
            this.setState({ status: true })
        }
    }

    onClickTeruskan() {

        fetch("http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/profile/" + this.state.userId, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
            },
            body: JSON.stringify({

                "status": "DONE"

            })
        })
            .then(response => response.json())
            .then(
            Alert.alert(
                'Pesan',
                'Update Berhasil',
                [
                    //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                    //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {
                        text: 'OK', onPress: () => {
                        }
                    },
                ],
                { cancelable: false }
            )
            )


    }


    render() {
        const {goBack} = this.props.navigation;
        {
            if (this.state.dataLaporanSelesai.rating == null) {
                    this.state.dataLaporanSelesai.rating = 0;
                }
        }
        
            //  if (this.state.dataLaporanSelesai.rating==5) {
            //     this.state.bintang5 = true
            //   //  this.state.dataLaporanSelesai.rating = 0;
                
            // }else if (this.state.dataLaporanSelesai.rating == 1) {
            //     this.state.bintang1 = true
            // }else if (this.state.dataLaporanSelesai.rating == 2) {
            //    this.state.bintang2 = true
            // }else if (this.state.dataLaporanSelesai.rating == 3) {
            //     this.state.bintang3 = true
            // }else if (this.state.dataLaporanSelesai.rating == 4) {
            //     this.state.bintang4 = true
            // }else if (this.state.dataLaporanSelesai.rating == 0) {
            //     this.state.bintang0 = true
            // }
            // this.setState({ refreshing: true });
        
       

        console.log("bintang 2: "+this.state.bintang1)

        return (
            <Container style={{ marginTop: 25, backgroundColor: "#fff" }}>
                <Header style={{backgroundColor:"white" }}>
                    <Left>
                       
                            <Icon name="arrow-back" onPress={() => goBack(null)} />
                      
                    </Left>
                    <Body style={{ backgroundColor: "#fff", alignItems: "center" }}>
                        <Title style={{ color: "#000" }}>Detail Laporan</Title>
                    </Body>
                    <Right />
                </Header>
               <Content>
                    <Image source={{ uri: this.state.dataLaporanSelesai.images }} style={{ height: 200, width: null, flex: 1 }} />
                    <Card>

                        <CardItem>
                            <Left>
                                <Thumbnail small source={{ uri: this.state.dataLaporanSelesai.avatar_pelapor }} />
                                <Body>
                                    <Text>{this.state.dataLaporanSelesai.pelapor}</Text>
                                    <Text style={{ fontSize: responsiveFontSize(1.5) }}>No Tiket : {this.state.dataLaporanSelesai.nomor_laporan}</Text>
                                </Body>
                            </Left>
                            <Right>
                                <Text style={{ fontStyle: "italic", fontSize: responsiveFontSize(1.5) }}> {this.state.dataLaporanSelesai.tanggal_laporan} </Text>
                            </Right>
                        </CardItem>

                        <CardItem>
                            <Text style={{ fontSize: 12 }}>{this.state.dataLaporanSelesai.description} </Text>
                        </CardItem>

                        <CardItem style={{ height: "25%" }}>

                            <View style={{ flex: 1, backgroundColor: "#fff", width: "100%", height: "100%" }}>
                                {/*<View style={{ backgroundColor: "#000", height: "40%", width: "100%" }}>
                                </View>*/}
                                
                                <View style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between'
                                }}>
                                    <View style={{ marginTop: "-2%", alignItems: 'center', flexDirection: 'row', width: "35%", height: "100%" }} >
                                        <View style={{ alignItems: 'center', marginLeft: "10%", justifyContent: 'center', flexDirection: 'row', width: "20%", height: "50%" }}>
                                            <Image style={{}} source={require("../../img/asset/ic-map.png")} />
                                        </View>
                                        <Text style={{ marginTop: "-2%", alignItems: 'center', marginLeft: "10%", fontSize: 10 }}>{this.state.dataLaporanSelesai.lokasi}</Text>
                                    </View>

                                    <View style={{ marginTop: "-2%", alignItems: 'center', flexDirection: 'row', width: "35%", height: "100%" }} >
                                        <View style={{ borderRadius: 100, backgroundColor: this.state.dataLaporanSelesai.icon, alignItems: 'center', marginLeft: "10%", justifyContent: 'center', flexDirection: 'row', width: "10%", height: "25%" }}>
                                            {/*<Image style={{ borderRadius: 100 }} source={require("../../img/asset/ic-menunggu.png")} />*/}
                                        </View>
                                        { this.state.dataLaporanSelesai.status == "selesai" ?
                                            <Text style={{ marginTop: "-2%", alignItems: 'center', marginLeft: "5%", fontSize: 10 }}>Selesai</Text> : null
                                        }
                                        

                                    </View>
                                </View>
                            </View>
                            {/*<View transparent style={{ flex: 1, height: "100%", flexDirection: 'row', width: "70%", left: "1%" }}>
                                <Image style={{}}
                                    source={require("../../img/asset/ic-map.png")} />
                                <Text note style={{ fontSize: responsiveFontSize(1.5), width: "70%", marginLeft: "5%", marginTop: "-2%" }}>{this.state.dataLaporanSelesai.lokasi}</Text>
                            </View>

                            <View transparent style={{ flex: 1, flexDirection: 'row', height: "100%", width: "30%", right: "1%", paddingTop: "5%" }}>
                                <Image style={{ height: "60%", width: "6%" }}
                                    source={require("../../img/asset/ic-menunggu.png")}
                                />
                                <Text note style={{ fontSize: responsiveFontSize(1.5), width: "70%", marginLeft: "5%", marginTop: "0%" }}>{this.state.dataLaporanSelesai.status}</Text>
                            </View>*/}
                        </CardItem>

                        <ListItem style={{ height: "15%" }}>
                            <Left>
                                <Text style={{ fontSize: 12 }}>Kategori</Text>
                            </Left>

                            <Right>
                                <Text style={{ fontSize: 12 }}>{this.state.dataLaporanSelesai.kategori}</Text>
                            </Right>
                        </ListItem>
                        <ListItem style={{ height: "15%" }}>
                            <Left>
                                <Text style={{ fontSize: 12 }}>Tag BUMN</Text>
                            </Left>


                            <Label style={{ textAlign: 'center', fontSize: responsiveFontSize(1.5), color: "black", backgroundColor: '#e8f0ff', marginRight: "1%", borderRadius: 2 }}>
                                {this.state.dataLaporanSelesai.nama_bumn}
                            </Label>

                        </ListItem>
                        <ListItem style={{ height: "15%", marginBottom : "5%" }}>
                            <Left>
                                <Text style={{ fontSize: 12 }}>Bukti Solusi</Text>
                            </Left>

                            {/*<TouchableOpacity onPress={() => this.props.navigation.navigate("LihatBukti", { nomor_laporan: this.state.dataLaporanSelesai.nomor_laporan })}>*/}
                            <Text  transparant onPress={() => this.props.navigation.navigate("LihatBukti", { nomor_laporan: this.state.dataLaporanSelesai.nomor_laporan })}
                                style={{fontSize: responsiveFontSize(1.5), marginRight: "1%",  borderRadius: 2 }}>
                                <Text style={{ color: "blue" }}>Lihat Bukti</Text>
                            </Text>
                        </ListItem>
                    </Card>


                    <View style={{ backgroundColor: "#f7f7f7", flex: 1, alignContent: "center", justifyContent: 'center', alignItems: 'center', marginLeft: "10%", marginRight: "10%", marginTop: "5%", marginBottom: "5%", borderRadius: 5 }}>
                        <Text style={{ marginTop: "5%", marginBottom: "5%" }}>
                            Status Selesai
                            </Text>
                    </View>

                    <View style={{ backgroundColor: "#f7f7f7", borderBottomWidth: 2, borderRadius: 5, borderColor: "#000", flex: 1, justifyContent: 'center', alignItems: 'center', marginLeft: "10%", marginRight: "10%", marginTop: "5%" }}>

                        <View style={{ marginTop: "3%", marginLeft: "5%", marginRight: "5%", marginBottom: "1%", width: "90%" }}>
                            <Text style={{ fontSize: responsiveFontSize(1.8), textAlign: "center", alignSelf: "center" }}>
                                Kata mereka tentang pekerjaan anda
                            </Text>
                        </View>

                        <View style={{ marginTop: "10%", flex: 1, justifyContent: 'center', flexDirection: 'row', alignItems: 'center', marginRight: "5%" }} >
                        
                         {
                                this.state.dataLaporanSelesai.rating == 0 || this.state.dataLaporanSelesai.rating == null ?
                                    <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
                                        <Image style={{}} source={require("../../img/asset/Star-abu.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-abu.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-abu.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-abu.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-abu.png")} />
                                    </View> : null

                            }

                            

                            {
                                this.state.dataLaporanSelesai.rating == 1 ?
                                    <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
                                        <Image style={{}} source={require("../../img/asset/Star-kuning.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-abu.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-abu.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-abu.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-abu.png")} />
                                    </View> : null

                            }

                            {
                                this.state.dataLaporanSelesai.rating == 2 ?
                                    <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
                                        <Image style={{}} source={require("../../img/asset/Star-kuning.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-kuning.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-abu.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-abu.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-abu.png")} />
                                    </View> : null

                            }

                            {
                                this.state.dataLaporanSelesai.rating == 3 ?
                                    <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
                                        <Image style={{}} source={require("../../img/asset/Star-kuning.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-kuning.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-kuning.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-abu.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-abu.png")} />
                                    </View> : null

                            }

                            {
                                this.state.dataLaporanSelesai.rating == 4 ?
                                    <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
                                        <Image style={{}} source={require("../../img/asset/Star-kuning.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-kuning.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-kuning.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-kuning.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-abu.png")} />
                                    </View> : null

                            }
                            {
                                this.state.dataLaporanSelesai.rating == 5 ?
                                    <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
                                        <Image style={{}} source={require("../../img/asset/Star-kuning.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-kuning.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-kuning.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-kuning.png")} />
                                        <Image style={{}} source={require("../../img/asset/Star-kuning.png")} />
                                    </View> : null

                            }
                            <Text style={{ fontSize: responsiveFontSize(1.8), marginLeft: "3%", fontWeight: "bold" }}>{this.state.dataLaporanSelesai.rating}/5</Text>

                        </View>


                        <View style={{ marginTop: "10%", marginBottom: "10%", marginLeft: "5%", marginRight: "5%" }}>

                            <Text style={{ alignSelf: "center" }}>
                                {this.state.dataLaporanSelesai.description}
                            </Text>
                        </View>

                    </View>

                </Content>

            </Container>

        );
    }

    cekstok() {

    }

    handleOpenWithLinking = () => {
        Linking.openURL('sms:/open?addresses=&body=\Hello, Please follow the following url: \http://google.com');
    }


}

const styles = StyleSheet.create({
    imgFooter: {
        height: "40%",
        width: "22%",
        marginBottom: "5%"
    },
    footer: {
        backgroundColor: "#fff"
    },
    fontFooter: {
        color: "#000",
        fontSize: responsiveFontSize(1.5)
    },
    oneButton: {
        height: 60,
        backgroundColor: '#e41c23',
    },

    oneButtonView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        height: 20,
        justifyContent: 'center',
    },

    payTextDollar: {
        flex: 1,
        color: '#ffffff',
        fontSize: 20,
        fontFamily: 'MetaOffc-Light',
        textAlign: 'center',
    },

    imageButton: {
        marginTop: 6,
    },
    flexDetailPengerjaan: {
        textAlign: 'center', fontSize: responsiveFontSize(1.5), color: "black", marginLeft: "15%", marginRight: "40%", borderRadius: 2
    }
});

