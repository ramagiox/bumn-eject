import React, { Component } from "react";
import { View,StyleSheet } from "react-native";
import { Container, Content, Picker, Button, Text } from "native-base";
import Expo from "expo";
import Footer from './src/footer/footer';
import HomeScreen from "./src/HomeScreen/index.js";
import Login from "./src/screen/index.js";
import LoginProfil from "./src/screen/loginProfil.js";
import Profil from "./src/screen/index.js";
import UbahProfil from "./src/screen/ubahProfil.js";
import UbahPassword from "./src/screen/ubahPassword.js";
import BeforeLogin from "./src/screen/index.js";
import Proses from "./src/tabRiwayat/index.js";
import Bantuan from "./src/screen/bantuan.js";
import AutocompleteExample from "./src/screen/autocomplete.js";

export default class App extends React.Component {
  constructor() { 
    super();
    this.state = {
      isReady: false
    };
  }
  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("native-base/Fonts/Ionicons.ttf")
    });
    this.setState({ isReady: true });
  }
  render() {
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    }
    return(

	<BeforeLogin/>


	);
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
